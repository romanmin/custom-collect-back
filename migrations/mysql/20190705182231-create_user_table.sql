
-- +migrate Up
CREATE TABLE IF NOT EXISTS users (
    id              INTEGER      AUTO_INCREMENT NOT NULL,
    login           VARCHAR(255)                NOT NULL,
    email           VARCHAR(255)                NOT NULL,
    password_hash   VARCHAR(255)                NOT NULL,
    status          INT(1)                      NOT NULL,
    PRIMARY KEY (id)
);
-- +migrate Down
