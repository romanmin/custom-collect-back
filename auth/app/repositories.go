package app

import "database/sql"

type (
	user struct {
		id    int    `json:"id"`
		email string `json:"email"`
		login string `json:"login"`
	}
	
	userRepository interface {
		createUser()
	}
	
	mysqlUserRepository struct {
		db *sql.DB
	}
	
)

func newMysqlUserRepository(db *sql.DB) *mysqlUserRepository {
	return &mysqlUserRepository{db: db}
}


func (ur *mysqlUserRepository) createUser (login string, email string, pwd string) error  {
	
	return nil
}


